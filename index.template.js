const Rollbot = require("./rollbot")
const config = require("./config")

// thread 1
var t1 = new Rollbot(config);
t1.init();

// thread 2
var t2_config = Object.assign({}, config, {
  thread: "{THREAD2ID}",
  // you can edit more config values here
});
var t2 = new Rollbot(t2_config);
t2.init();
